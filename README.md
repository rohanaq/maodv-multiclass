# M-AODV - Multiclass

## Multiclass On-demand Routing in Heterogeneous Ad Hoc Networks

     Rohana Qudus
     0511154000045
     Jaringan Nirkabel 2018
---
## Outline
1. [Penjelasan](#penjelasan)
2. [Modifikasi](#modifikasi)
3. [Referensi](#referensi)

## Penjelasan
Modifikasi yang dilakukan berada pada proses _route discovery_.  Modifikasi ini memperhitungkan heterogenitas jaringan dan memperkenalkan strategi untuk secara efisien menggunakan sumber daya yang tersedia. _Route discovery_ pada MCAODV mendukung dua layanan kelas yang berbeda, _realtime_ dan _non-realtime_.
<br>
![modified-header](images/header.png)
<br>
Pada RREQ message format ditambahkan T sebagai penanda jenis layanan yang digunakan (1 untuk _realtime_ dan 0 untuk _non-realtime_). Selain itu juga ditambahkan _Cost_ untuk menyimpan nilai _route cost_ berdasarkan perhitungan sebagai berikut:
    - Untuk komunikasi realtime menggunakan rumus:
        `Cost = α(Delay) + β(DataRate) + γ(HopCount)`
    - Untuk komunikasi non-realtime menggunakan rumus:
        `Cost = α(Hopcount) + β(Loadfactor)`
<br><br>
Modifikasi alur pada proses _route discovery_:
<br>
![flowchart](images/flow.png)
<br>
Sewaktu _node_ menerima _route request_, jenis layanan akan dicek apakah _realtime_ atau _non-realtime_. Setelah paket yang masuk dicek jenis layanannya, nilai _cost_ pada RREQ Format akan diperbarui menggunakan rumus yang sesuai dengan jenis layanannya. Selain pada RREQ Format, nilai _cost_ juga disimpan pada _routing table_ untuk dibandingkan pada langkah selanjutnya. Jika nilai _cost_ dari paket yang masuk lebih tinggi daripada nilai _cost_ yang ada pada _routing table_ maka paket akan di-_drop_, sebaliknya jika nilai _cost_ lebih kecil maka nilai _cost_ pada _routing table_ akan diperbarui dengan nilai yang lebih kecil.

## Modifikasi
1. Menambahkan field _T_ dan _cost_ pada RREQ Message Format
    - _T_ sebagai penanda jenis layanan yang digunakan
    - _cost_ digunakan untuk menyimpan nilai _route cost_

2. Menambahkan field _cost_ pada _routing table_

3. Melakukan inisialisasi paket yang dikirim pada fungsi **AODV::sendRequest** yang ada di **aodv.cc**
    - **rq_serviceClass** diberi nilai 0
    - **rq_cost** diberi nilai 0

4. Melakukan cek jenis layanan yang digunakan kemudian menghitung _route cost_ dengan menambahkan modifikasi pada fungsi **AODV::recvRequest** yang ada di **aodv.cc**
    - Untuk komunikasi realtime menggunakan rumus:
        `Cost = α(Delay) + β(DataRate) + γ(HopCount)`
    - Untuk komunikasi non-realtime menggunakan rumus:
        `Cost = α(Hopcount) + β(Loadfactor)`

5. Membandingkan _route cost_ terbaru dengan yang sudah ada pada _routing table_. Modifikasi dilakukan pada fungsi **AODV::recvRequest** yang ada di **aodv.cc**
    - Jika _route cost_ terbaru lebih besar maka paket akan didrop
    - Sebaliknya maka nilai _route cost_ pada _routing table_ akan diupdate

## Referensi
- https://ieeexplore.ieee.org/document/6866717
- https://arxiv.org/pdf/1007.4065.pdf 
- https://intip.in/RA2018/